﻿using Dashboard.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace Dashboard.Controllers
{
    public class DashboardController : Controller
    {
        private readonly ILogger<DashboardController> _logger;

        public DashboardController(ILogger<DashboardController> logger)
        {
            _logger = logger;
        }

        [Route("Team/{ip?}")]
        public IActionResult Team(long? ip)
        {
            TeamViewModel viewData = SqliteDataAccess.GetHistoryData(ip, SqliteDataAccess.LabelType.Ip) as TeamViewModel;
            return View(viewData);
        }

        [HttpGet]
        [Route("")]
        [Route("Results/{devArea?}/{group?}/{ip?}/{view?}")]
        public IActionResult Results(long? devArea, long? group, long? ip, long? view)
        {
            ResultViewModel viewData = SqliteDataAccess.GetResultsData(devArea, group, ip, view);
            return View(viewData);
        }

        [Route("Build/{group?}")]
        public IActionResult Build(long? group)
        {
            HistoryViewModel viewData = SqliteDataAccess.GetHistoryData(group, SqliteDataAccess.LabelType.Group);
            return View(viewData);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
