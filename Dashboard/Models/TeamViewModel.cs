﻿using Nancy.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dashboard.Models
{
    public class TeamViewModel : HistoryViewModel
    {
        // contains 3 arrays that will fill the bar chart
        public ChartData Data { get; private set; }
        // used for serializing the arrays in ChartData (see "Views/Dashboard/team.cshtml")
        public JavaScriptSerializer jsSerializer = new JavaScriptSerializer();


        public TeamViewModel(List<LabelModel> teams, string selectedTeam, List<TestHistoryModel> history) 
            : base(teams, selectedTeam, history)
        {
            SetChartData();
        }

        public void SetChartData()
        {
            /*
                This method will split the data in the "statistics" dictionary into arrays,
                so they it can be displayed in the bar chart. As it fills the 3 arrays 
                step by step, it will also look for the highest number of passed or failed
                tests found in a single day, so that it can tell the bar chart what the 
                max value should be.
             */

            var statistics = GetStatistics();
            Data = new ChartData(statistics.Count);
            int index = 0;

            foreach (KeyValuePair<DateTime, ResultsSet> pair in statistics)
            {
                long passed = pair.Value.passed;
                long failed = pair.Value.failed;

                Data.labels[index] = pair.Key.ToShortDateString();
                Data.passed[index] = passed;
                Data.failed[index] = failed;

                if (passed > Data.maxValue)
                    Data.maxValue = passed;

                if (failed > Data.maxValue)
                    Data.maxValue = failed;

                index++;
            }
        }

        private SortedDictionary<DateTime, ResultsSet> GetStatistics()
        {
            /* This method will take the data from the "history" list (the relevant variables 
               are TestHistoryModel.date and TestHistoryModel.status) and fill the 
               "statistics" dictionary so that, for each date (the key), it will contain the number 
               of passed and failed tests (the value). */

            var statistics = new SortedDictionary<DateTime, ResultsSet>();
            foreach (var h in history)
            {
                var key = h.date;
                if (!statistics.ContainsKey(key))
                {
                    statistics[key] = new ResultsSet();
                }

                switch (h.status)
                {
                    case "pass":
                        {
                            statistics[key].passed++;
                            break;
                        }
                    case "fail":
                        {
                            statistics[key].failed++;
                            break;
                        }
                }
            }

            return statistics;
        }


        /* This class is used for storing data for the bar chart in the Team view.*/
        public class ChartData
        {
            public string[] labels; // the dates, which will be used as labels
            public long[] passed;
            public long[] failed;
            public long maxValue;

            public ChartData(int size)
            {
                labels = new string[size];
                passed = new long[size];
                failed = new long[size];
                maxValue = 0;
            }
        }
    }
}
