﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.Models
{
    public class SequenceModel : Identifiable
    {
        public List<TestModel> Tests { get; private set; }


        public SequenceModel(long id, string name, List<TestModel> tests) : base(id, name)
        {
            Tests = tests;
        }
    }
}
