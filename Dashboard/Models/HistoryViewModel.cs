﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard.Models
{
    public class HistoryViewModel
    {
        // list used for the drop-down button list
        public readonly List<LabelModel> labels;
        // the currently selected entity that will be displayed on the dropdown button
        public readonly string selectedLabel;

        // list used for filling the table rows
        public readonly List<TestHistoryModel> history;

        public HistoryViewModel(List<LabelModel> labels, string selectedLabel, List<TestHistoryModel> history)
        {
            this.labels = labels;
            this.selectedLabel = selectedLabel;

            this.history = (from h in history
                            orderby h.date
                            select h).ToList();
        }
    }
}
