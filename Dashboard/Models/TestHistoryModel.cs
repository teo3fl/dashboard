﻿using System;

namespace Dashboard.Models
{
    public class TestHistoryModel
    {
        public readonly string sequence;
        public readonly string test;
        public readonly string status;
        public readonly string platform;
        public readonly DateTime date;

        public TestHistoryModel(string sequence, string test, string status, string platform, string date)
        {
            this.sequence = sequence;
            this.test = test;
            this.status = status;
            this.platform = platform;
            this.date = DateTime.Parse(date);
        }
    }
}
