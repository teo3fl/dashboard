﻿
using System;
using System.Collections.Generic;

namespace Dashboard.Models
{
    public class TestModel : Identifiable
    {
        public readonly string currentWinStatus;
        public readonly string currentLnxStatus;

        public TestModel(long id, string name, string currentWinStatus, string currentLnxStatus) : base(id, name)
        {
            this.currentWinStatus = currentWinStatus;
            this.currentLnxStatus = currentLnxStatus;
        }
    }
}
