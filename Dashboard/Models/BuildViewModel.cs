﻿using System.Collections.Generic;

namespace Dashboard.Models
{
    public class BuildViewModel : HistoryViewModel
    {
        public BuildViewModel(List<LabelModel> builds, string selectedBuild, List<TestHistoryModel> history)
            : base(builds, selectedBuild, history)
        {

        }
    }
}
