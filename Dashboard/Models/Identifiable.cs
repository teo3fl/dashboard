﻿
namespace Dashboard.Models
{
    public abstract class Identifiable
    {
        public readonly long ID;
        public readonly string name;

        public Identifiable(long id, string name)
        {
            ID = id;
            this.name = name;
        }
    }
}
