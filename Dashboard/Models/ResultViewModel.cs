﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Dashboard.Models
{
    public class ResultViewModel
    {
        // these lists will fill the corresponding dropdown buttons
        public Dictionary<SqliteDataAccess.LabelType, List<LabelModel>> LabelLists { get; private set; }

        // the DevelopmentArea, IP, Group, and View that correspond to the selected Sequences
        public Dictionary<SqliteDataAccess.LabelType, long> selectedLabels;

        // list of sequences that resulted after filtering 
        public List<SequenceModel> Sequences { get; private set; }

        // doughnut charts data
        public ResultsSet WinResults { get; private set; }
        public ResultsSet OverallResults { get; private set; }

        public DateTime LastUpdated { get; private set; }


        public ResultViewModel(Dictionary<SqliteDataAccess.LabelType, List<LabelModel>> labelLists,
            Dictionary<SqliteDataAccess.LabelType, long> selectedLabels,
            List<SequenceModel> sequences, ResultsSet winResults, ResultsSet overallResults)
        {
            LabelLists = labelLists;
            this.selectedLabels = selectedLabels;
            Sequences = sequences;

            OverallResults = overallResults;
            WinResults = winResults;

            LastUpdated = FileParser.LastUpdated;
        }

        public LabelModel GetSelectedLabel(SqliteDataAccess.LabelType labelType)
        {
            var label = (from l in LabelLists[labelType]
                         where l.ID == selectedLabels[labelType]
                         select l).First();

            return label;
        }
    }
}
