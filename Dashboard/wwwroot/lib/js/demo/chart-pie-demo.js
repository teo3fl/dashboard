// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

function loadDonutChart(id, regressed, failed, passed) {
    var ctx = document.getElementById(id);
    var myPieChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: ["Regression", "Failed", "Passed"],
            datasets: [{
                data: [regressed, failed, passed],
                backgroundColor: ['#e74a3b', '#f6c23e', '#1cc88a'],
                hoverBackgroundColor: ['#b2291c', '#e1ac27', '#009b63'],
                hoverBorderColor: "rgba(234, 236, 244, 1)",
            }],
        },
        options: {
            maintainAspectRatio: false,
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                caretPadding: 10,
            },
            legend: {
                display: false
            },
            cutoutPercentage: 80,
        },
    });
}