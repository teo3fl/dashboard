﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dashboard
{
    public class ResultsSet
    {
        public long passed;
        public long failed;
        public long regressed;

        public static ResultsSet operator +(ResultsSet a, ResultsSet b)
        => new()
        { passed = a.passed + b.passed, failed = a.failed + b.failed, regressed = a.regressed + b.regressed };
    }
}
