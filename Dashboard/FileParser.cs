﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;

namespace Dashboard
{
    public class FileParser
    {
        private static Regex resultsRegex = new Regex(@".*results.*xml");

        private static DateTime lastUpdated;
        public static DateTime LastUpdated
        {
            get { return lastUpdated; }
            set
            {
                lastUpdated = value;
                SqliteDataAccess.InsertKeyValue("lastUpdated", lastUpdated.Ticks.ToString());
            }
        }

        public static string ResultsFolder { get; set; }
        public static string SeqFolder { get; set; }


        public static void InitializeLastUpdated(string date)
        {
            try
            {
                lastUpdated = new DateTime(long.Parse(date));
            }
            catch
            {
                lastUpdated = DateTime.MinValue;
            }
        }

        public static bool CanUpdate()
        {
            var files = GetResultsFiles();

            foreach (string file in files)
            {
                if (DateTime.Compare(File.GetLastWriteTime(file), LastUpdated) > 0)
                {
                    return true;
                }
            }

            return false;
        }

        public static List<XmlData> ParseFiles()
        {
            var testResults = ParseTestResults(ResultsFolder);
            SetSequenceViews(ref testResults, SeqFolder);

            return testResults;
        }


        private static List<string> GetResultsFiles()
        {
            return Directory.GetFiles(ResultsFolder, "*.xml", SearchOption.AllDirectories)
                                 .Where(path => resultsRegex.IsMatch(path))
                                 .ToList();
        }

        private static List<string> GetUnparsedFiles()
        {
            /* This method will look through the files in the results folder and return the ones that
               have not been parsed yet. It will also update the "LastUpdated" variable so that it
               will be equal to the last write date of the newest modified file. */

            var files = GetResultsFiles();

            var unparsedFiles = new List<string>();

            foreach (string file in files)
            {
                var lastWriteTime = File.GetLastWriteTime(file);
                if (DateTime.Compare(lastWriteTime, LastUpdated) > 0)
                {
                    unparsedFiles.Add(file);
                }
            }

            return unparsedFiles;
        }


        // .xml files

        private static List<XmlData> ParseTestResults(string resultsFolder)
        {
            var files = GetUnparsedFiles();

            List<XmlData> data = new List<XmlData>();
            DateTime newestFileWriteTime = LastUpdated;

            foreach (string file in files)
            {
                var xmlData = ParseTest(file);
                data.Add(xmlData);

                var lastWriteTime = File.GetLastWriteTime(file);
                if (DateTime.Compare(lastWriteTime, newestFileWriteTime) > 0)
                {
                    newestFileWriteTime = lastWriteTime;
                }
            }

            LastUpdated = newestFileWriteTime;
            return data;
        }

        private static XmlData ParseTest(string filePath)
        {
            XmlData xmlData = null; // custom class that contains relevant data extracted from a test result xml
            try
            {
                xmlData = new XmlData();

                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(filePath);

                // extract data from the "Info" node
                var infoNodes = xDoc.DocumentElement.ChildNodes[0];
                foreach (XmlNode node in infoNodes)
                {
                    switch (node.Name)
                    {
                        case "Platform":
                            {
                                xmlData.Sequence.Platform = node.InnerText;
                                break;
                            }
                        case "Environment":
                            {
                                xmlData.Sequence.Environment = node.InnerText;
                                break;
                            }
                        case "RunLabel":
                            {
                                xmlData.Sequence.Label = node.InnerText;
                                break;
                            }
                        case "DbaseName":
                            {
                                xmlData.Sequence.DbaseName = node.InnerText;
                                break;
                            }
                    }
                }

                // extract data from the "ResultsSet" node, which contains one or more "Results" nodes
                var resultSetNode = xDoc.DocumentElement.ChildNodes[1];
                foreach (XmlNode resultNode in resultSetNode)
                {
                    XmlData.Test testData = new XmlData.Test();
                    foreach (XmlNode node in resultNode)
                    {
                        switch (node.Name)
                        {
                            case "seq_name":
                                {
                                    if (xmlData.Sequence.SeqName == null)
                                    {
                                        xmlData.Sequence.SeqName = node.InnerText;
                                    }

                                    break;
                                }
                            case "uft_name":
                                {
                                    testData.Name = node.InnerText;
                                    break;
                                }
                            case "status":
                                {
                                    testData.Status = node.InnerText;
                                    break;
                                }
                            case "date":
                                {
                                    testData.Date = node.InnerText;
                                    break;
                                }
                        }
                    }

                    // add the test data to the XmlData object
                    xmlData.Tests.Add(testData);
                }
            }
            catch { }

            return xmlData;
        }


        // .seq files

        private static void SetSequenceViews(ref List<XmlData> testResults, string seqFolder)
        {
            var files = Directory.GetFiles(seqFolder, "*.seq", SearchOption.AllDirectories);

            foreach (string file in files)
            {
                var tuple = GetSequenceViewPair(file);
                string seqName = tuple.Item1;
                string view = tuple.Item2;

                var tests = from t in testResults
                            where t.Sequence.SeqName == seqName
                            select t;

                foreach (XmlData test in tests)
                {
                    test.Sequence.View = view;
                }
            }
        }

        private static Tuple<string, string> GetSequenceViewPair(string file)
        {
            string seqName;
            string viewName;

            string[] lines = File.ReadAllLines(file);

            seqName = lines[6].Remove(0, "# SEQUENCE_FILE_NAME:".Length).Trim();
            seqName = seqName.Remove(seqName.LastIndexOf(".seq")); // remove the file extension from the sequence name
            viewName = lines[14].Remove(0, "# TAC_VIEW:".Length).Trim();

            return Tuple.Create(seqName, viewName);
        }
    }
}

