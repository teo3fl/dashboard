﻿using Dashboard.Models;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;

namespace Dashboard
{
    public class SqliteDataAccess
    {
        public static string ConnectionString { get; set; }
        private static readonly string storageTableName = "Storage";

        public enum LabelType
        {
            DevelopmentArea,
            Group,
            Ip,
            View
        }

        public enum Platform
        {
            win64, lnx
        }

        /* Database Structure:
         
           Label entities:
            - DevelopmentArea(PK "id", "name")
            - Group(PK "id", "name")
            - Ip(PK "id", "name")
            - View(PK "id", "name")

           Test-related entities:
            - Sequence (PK "id", "name", FK "developmentAreaId", FK "viewId", FK "ipId", FK "groupId")
            - Test (PK "id", "name", FK "sequenceId", "currentStatus")
            - TestHistory (FK "testId", "status", "date")

            Storage ("key", "value")

         */


        /* Returns data for the Results view. */
        public static ResultViewModel GetResultsData(long? devArea, long? group, long? ip, long? view)
        {
            CheckForUpdates();

            var labels = new Dictionary<LabelType, List<LabelModel>>();
            foreach (LabelType labelType in Enum.GetValues(typeof(LabelType)))
            {
                if (labelType == LabelType.Group)
                {
                    long ipId = ip == null ? GetFirstLabelId(LabelType.Ip) : (long)ip;
                    labels[LabelType.Group] = GetGroupsForIp(ipId);
                }
                else
                {
                    labels[labelType] = GetLabels(labelType);
                }
            }

            var selectedLabels = GetSelectedLabels(labels, devArea, group, ip, view);
            var sequences = GetSequences(selectedLabels);
            var winResults = GetPlatformResults(selectedLabels[LabelType.DevelopmentArea], Platform.win64);
            var overallResults = GetPlatformResults(selectedLabels[LabelType.DevelopmentArea], Platform.lnx) + winResults;

            return new ResultViewModel(labels, selectedLabels, sequences, winResults, overallResults);
        }

        /* Returns data for a view that contains test history. */
        public static HistoryViewModel GetHistoryData(long? id, LabelType labelType)
        {
            CheckForUpdates();

            long labelId = id != null || id < 0 ? (long)id : GetFirstLabelId(labelType);
            var history = GetHistory(labelId, labelType);
            var labels = GetLabels(labelType);
            string labelName = (from t in labels
                                where t.ID == labelId
                                select t.name).First();

            switch (labelType)
            {
                case LabelType.Ip:
                    {
                        return new TeamViewModel(labels, labelName, history);
                    }
                case LabelType.Group:
                    {
                        return new HistoryViewModel(labels, labelName, history);
                    }
                default:
                    {
                        throw new Exception("SqliteDataAccess.GetHistoryData(): invalid label type.");
                    }
            }
        }


        /* Returns a dictionary of IDs for the selected labels (DevelopmentArea, Group, etc.). 
           The dictionary is used in the Results view for filling the lists of the drop-down buttons. */
        private static Dictionary<LabelType, long> GetSelectedLabels(Dictionary<LabelType, List<LabelModel>> labels,
            long? devArea, long? group, long? ip, long? view)
        {
            var selectedLabels = new Dictionary<LabelType, long>();

            // if the given parameters are null, a default label will be chosen from the "labels" dictionary

            selectedLabels[LabelType.DevelopmentArea] = devArea == null ?
                labels[LabelType.DevelopmentArea][0].ID : (long)devArea;

            selectedLabels[LabelType.Ip] = ip == null ?
                labels[LabelType.Ip][0].ID : (long)ip;

            selectedLabels[LabelType.View] = view == null ?
                labels[LabelType.View][0].ID : (long)view;


            // when the IP changes, the currently selected group might not be included in the
            // list of groups associated to that IP

            try
            {
                long groupId = (long)group; // will throw an exception if it is null
                var results = (from l in labels[LabelType.Group]
                               where l.ID == groupId
                               select l).First(); // will throw an exception if the sequence contains no elements

                selectedLabels[LabelType.Group] = results.ID;
            }
            catch (Exception e)
            {
                selectedLabels[LabelType.Group] = labels[LabelType.Group][0].ID;
            }

            return selectedLabels;
        }


        private static void CheckForUpdates()
        {
            if (FileParser.CanUpdate())
            {
                UpdateDatabase(FileParser.ParseFiles());
            }
        }

        private static void UpdateDatabase(List<XmlData> data)
        {
            foreach (XmlData sequence in data)
            {
                UpdateSequenceData(sequence);
            }
        }

        /* Updates data for a sequence and its tests. Might insert or update, depending on whether the
           sequence or the tests already exist in the database or not.*/
        private static void UpdateSequenceData(XmlData data)
        {
            /*
             if sequence already exists
                  foreach test
                      if the test already exists
                          update its current status 
                      else
                          insert the test
                      update the test history
             else
                  get labels ids (development area, group, etc.)
                  insert sequence
                  foreach test
                      insert test
                      update test history
            */

            long seqId = GetEntityId("Sequence", data.Sequence.SeqName);

            if (seqId > 0) // the id is greater than 0 if the database contains the sequence
            {
                foreach (var test in data.Tests)
                {
                    var platform = (Platform)Enum.Parse(typeof(Platform), data.Sequence.Platform);
                    var testData = GetTestData(test.Name, platform);
                    long testId;
                    if (testData != null)
                    {
                        testId = testData.Item1;
                        var (_, formerTestStatus, formerRegressionStatus) = testData;

                        /* 
                           A test is (still) regressed in 2 cases:
                            1. its former regression status was True and the new status is "fail"
                            2. its former status status was "pass" and the new status is "fail"
                        */
                        bool newRegression = ((bool)formerRegressionStatus || formerTestStatus == "pass") && test.Status == "fail";
                        UpdateTest(testId, test.Status, (Platform)Enum.Parse(typeof(Platform), data.Sequence.Platform), newRegression);
                    }
                    else
                    {
                        testId = InsertTestResult(test.Name, seqId, test.Status, data.Sequence.Platform);
                    }

                    InsertTestHistory(testId, test.Status, data.Sequence.Platform, test.Date);
                }
            }
            else
            {
                Dictionary<LabelType, long> labelIds = GetLabelsIds(data.Sequence);
                seqId = InsertTestSequence(
                    data.Sequence.SeqName,
                    labelIds[LabelType.DevelopmentArea],
                    labelIds[LabelType.View],
                    labelIds[LabelType.Ip],
                    labelIds[LabelType.Group]);

                foreach (var test in data.Tests)
                {
                    long testId = InsertTestResult(test.Name, seqId, test.Status, data.Sequence.Platform);
                    InsertTestHistory(testId, test.Status, data.Sequence.Platform, test.Date);
                }
            }
        }


        // select 

        /* Used for accessing the Storage table. */
        public static string GetKeyValue(string key)
        {
            string value = "";

            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                using (SQLiteCommand selectSql = new SQLiteCommand($"SELECT value FROM {storageTableName} WHERE key = @Key", conn))
                {
                    selectSql.Parameters.AddWithValue("Key", key);
                    conn.Open();

                    try
                    {
                        using (SQLiteDataReader r = selectSql.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                value = (string)r["value"];
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        // couldn't find a row
                    }
                }
            }

            return value;
        }

        /* Returns the ID of a given entity (Label, Sequence, Test) based on its name, or -1 if 
           none can be found. */
        private static long GetEntityId(string tableName, string entityName)
        {
            long id = -1;

            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                using (SQLiteCommand selectSql = new SQLiteCommand($"SELECT id FROM [{tableName}] WHERE name = @EntityName", conn))
                {
                    selectSql.Parameters.AddWithValue("EntityName", entityName);
                    conn.Open();

                    try
                    {
                        using (SQLiteDataReader r = selectSql.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                id = (long)r["id"];
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        // couldn't find a row
                    }
                }
            }

            return id;
        }

        /* Returns the IDs of all labels (Develompent Area, Group, View, IP) associated to a sequence. */
        private static Dictionary<LabelType, long> GetLabelsIds(XmlData.TestSequence sequence)
        {
            var labelsIds = new Dictionary<LabelType, long>();

            foreach (LabelType labelType in Enum.GetValues(typeof(LabelType)))
            {
                string labelName = "";

                switch (labelType)
                {
                    case LabelType.DevelopmentArea:
                        {
                            labelName = sequence.DbaseName;
                            break;
                        }
                    case LabelType.Group:
                        {
                            labelName = sequence.Label;
                            break;
                        }
                    case LabelType.Ip:
                        {
                            labelName = sequence.Environment;
                            break;
                        }
                    case LabelType.View:
                        {
                            labelName = sequence.View;
                            break;
                        }
                }

                long labelId = GetEntityId(labelType.ToString(), labelName);
                if (labelId == -1) // if the current label (Development Area, View, IP, Group) doesn't exist, it will be inserted
                {
                    labelId = InsertLabel(labelType.ToString(), labelName);
                }

                labelsIds[labelType] = labelId;
            }

            return labelsIds;
        }

        /* Returns a list of all labels (ID and name) contained in the database for the given 
           label type (DevelopmentArea, Group, etc.). */
        private static List<LabelModel> GetLabels(LabelType labelType)
        {
            List<LabelModel> labels = new List<LabelModel>();

            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                using (SQLiteCommand selectSql = new SQLiteCommand($"SELECT * FROM [{labelType}]", conn))
                {
                    conn.Open();

                    try
                    {
                        using (SQLiteDataReader r = selectSql.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                labels.Add(new LabelModel((long)r["id"], (string)r["name"]));
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        // couldn't find a row
                    }
                }
            }

            return labels;
        }

        /* Returns a list of sequences correspond to the given labels (DevelopmentArea, Group, 
           etc.). The list is used in the Results view for filling the table. */
        private static List<SequenceModel> GetSequences(Dictionary<LabelType, long> labelIds)
        {
            var sequences = new List<SequenceModel>();

            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                using (SQLiteCommand selectSql = new SQLiteCommand($"SELECT id, name FROM [sequence] WHERE developmentAreaId=@DevAreaId AND viewId=@ViewId AND ipId=@IpId AND groupId=@GroupId", conn))
                {
                    selectSql.Parameters.AddWithValue("DevAreaId", labelIds[LabelType.DevelopmentArea]);
                    selectSql.Parameters.AddWithValue("ViewId", labelIds[LabelType.View]);
                    selectSql.Parameters.AddWithValue("IpId", labelIds[LabelType.Ip]);
                    selectSql.Parameters.AddWithValue("GroupId", labelIds[LabelType.Group]);

                    conn.Open();

                    try
                    {
                        using (SQLiteDataReader r = selectSql.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                long id = (long)r["id"];
                                sequences.Add(new SequenceModel(id, (string)r["name"], GetTests(id, conn)));
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        // couldn't find a row
                    }
                }
            }

            return sequences;
        }

        /* Returns a list of tests that belong to the given sequence. */
        private static List<TestModel> GetTests(long sequenceId, SQLiteConnection conn)
        {
            var tests = new List<TestModel>();

            using (SQLiteCommand selectSql = new SQLiteCommand($"SELECT id, name, currentWinStatus, currentLnxStatus FROM [test] WHERE sequenceId=@SeqId", conn))
            {
                selectSql.Parameters.AddWithValue("SeqId", sequenceId);

                try
                {
                    using (SQLiteDataReader r = selectSql.ExecuteReader())
                    {
                        while (r.Read())
                        {
                            long id = (long)r["id"];
                            string name = (string)r["name"];
                            string winStatus = r["currentWinStatus"] is System.DBNull ? "" : (string)r["currentWinStatus"];
                            string lnxStatus = r["currentLnxStatus"] is System.DBNull ? "" : (string)r["currentLnxStatus"];

                            tests.Add(new TestModel(id, name, winStatus, lnxStatus));
                        }
                    }
                }
                catch (Exception e)
                {
                    // couldn't find a row
                }
            }

            return tests;
        }

        /* Returns a list that contains the history of tests where the sequence are associated with a
           certain label (DevelopmentArea, Ip, etc). The list is used in the history views. */
        private static List<TestHistoryModel> GetHistory(long labelId, LabelType label)
        {
            var history = new List<TestHistoryModel>();

            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                using (SQLiteCommand selectSql = new SQLiteCommand($"SELECT Sequence.name AS seqName, Test.name AS testName, TestHistory.status, TestHistory.platform, TestHistory.date FROM Sequence INNER JOIN Test ON Sequence.id = Test.sequenceId INNER JOIN TestHistory ON Test.id = TestHistory.testId WHERE Sequence.{label}Id = @GivenId; ", conn))
                {
                    selectSql.Parameters.AddWithValue("GivenId", labelId);
                    conn.Open();

                    try
                    {
                        using (SQLiteDataReader r = selectSql.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                history.Add(new TestHistoryModel((string)r["seqName"], (string)r["testName"], (string)r["status"], (string)r["platform"], (string)r["date"]));
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        // couldn't find a row
                    }
                }
            }

            return history;
        }

        /* Returns the id of the first label (DevelopmentArea, Group, etc.) in the database, or -1 
           if the table is empty. */
        private static long GetFirstLabelId(LabelType label)
        {
            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                using (SQLiteCommand selectSql = new SQLiteCommand($"SELECT id FROM [{label}]", conn))
                {
                    conn.Open();

                    try
                    {
                        using (SQLiteDataReader r = selectSql.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                return (long)r["id"];
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        // couldn't find a row
                    }
                }
            }

            return -1;
        }

        /* Returns a list of Groups associated with a given IP. Used for getting the data for 
           the "Group" drop-down in the Results view. */
        private static List<LabelModel> GetGroupsForIp(long ip)
        {
            List<LabelModel> groupLabels = new List<LabelModel>();

            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                using (SQLiteCommand selectSql = new SQLiteCommand($"SELECT DISTINCT [Group].id, [Group].name FROM Sequence INNER JOIN [Group] ON [Group].id = Sequence.groupId WHERE Sequence.ipId=@IpId", conn))
                {
                    selectSql.Parameters.AddWithValue("IpId", ip);
                    conn.Open();

                    try
                    {
                        using (SQLiteDataReader r = selectSql.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                groupLabels.Add(new LabelModel((long)r["id"], (string)r["name"]));
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        // couldn't find a row
                    }
                }
            }

            return groupLabels;
        }

        /* Returns a tuple that contains 3 elements: the ID of the given test, the current status for
           the given platform, and the regression status for said platform. */
        private static Tuple<long, string?, bool> GetTestData(string testName, Platform platform)
        {
            string statusField = platform == Platform.win64 ? "currentWinStatus" : "currentLnxStatus";
            string regressionField = platform == Platform.win64 ? "regressedWin" : "regressedLnx";

            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                using (SQLiteCommand selectSql = new SQLiteCommand($"SELECT id, {statusField}, {regressionField} FROM [Test] WHERE name = @EntityName", conn))
                {
                    selectSql.Parameters.AddWithValue("EntityName", testName);
                    conn.Open();

                    try
                    {
                        using (SQLiteDataReader r = selectSql.ExecuteReader())
                        {
                            if (r.Read())
                            {
                                var id = (long)r["id"];
                                var status = r[statusField] is System.DBNull ? null : (string)r[statusField];
                                var regressed = r[regressionField] is System.DBNull ? false : (long?)r[regressionField] == 1;

                                return Tuple.Create(id, status, regressed);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        // couldn't find a row
                    }
                }
            }
            return null;
        }

        /* Returns a set of numbers that count the passed, failed, and regressed tests for the given
           DevelopmentArea and platform. */
        private static ResultsSet GetPlatformResults(long developmentArea, Platform platform)
        {
            return new ResultsSet
            {
                passed = GetStatusCount(developmentArea, platform, "pass"),
                failed = GetStatusCount(developmentArea, platform, "fail"),
                regressed = GetRegressionCount(developmentArea, platform)
            };
        }

        /* Counts the number of tests that have a given status, associated with a given platform and Development Area.*/
        private static long GetStatusCount(long developmentArea, Platform platform, string status)
        {
            string statusField = platform == Platform.win64 ? "currentWinStatus" : "currentLnxStatus";
            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                using (SQLiteCommand selectSql = new SQLiteCommand($"SELECT COUNT(*) FROM Test INNER JOIN Sequence ON Sequence.id = Test.sequenceId WHERE developmentAreaId = @DevAreaId AND {statusField} = @Status", conn))
                {
                    selectSql.Parameters.AddWithValue("Status", status);
                    selectSql.Parameters.AddWithValue("DevAreaId", developmentArea);
                    conn.Open();

                    try
                    {
                        return (long)selectSql.ExecuteScalar();
                    }
                    catch (Exception e)
                    {
                        // couldn't find a row
                    }
                }
            }

            return 0;
        }

        /* Counts the number of regressed tests that have a given platform and Development Area.*/
        private static long GetRegressionCount(long developmentArea, Platform platform)
        {
            string regressionField = platform == Platform.win64 ? "regressedWin" : "regressedLnx";
            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                using (SQLiteCommand selectSql = new SQLiteCommand($"SELECT COUNT(*) FROM Test INNER JOIN Sequence ON Sequence.id = Test.sequenceId WHERE developmentAreaId = @DevAreaId AND {regressionField} = true", conn))
                {
                    selectSql.Parameters.AddWithValue("DevAreaId", developmentArea);
                    conn.Open();

                    try
                    {
                        return (long)selectSql.ExecuteScalar();
                    }
                    catch (Exception e)
                    {
                        // couldn't find a row
                    }
                }
            }

            return 0;
        }

        // insert

        /* Insert data into the Storage table if the given key doesn't exist. If it does,
           it updates the value. */
        public static void InsertKeyValue(string key, string value)
        {
            try
            {
                UpdateKeyValue(key, value);
            }
            catch
            {
                using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
                {
                    using (SQLiteCommand updateSql = new SQLiteCommand($"UPDATE {storageTableName} SET value=@Value WHERE key=@Key", conn))
                    {
                        updateSql.Parameters.AddWithValue("Key", key);
                        updateSql.Parameters.AddWithValue("Value", value);

                        conn.Open();

                        try
                        {
                            updateSql.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                }
            }
        }

        /* Creates new labels (DevelopmentArea, Group, View, Ip). */
        private static long InsertLabel(string tableName, string labelName)
        {
            long rowId;

            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                using (SQLiteCommand insertSQL = new SQLiteCommand($"INSERT INTO [{tableName}] (name) VALUES (?)", conn))
                {
                    insertSQL.Parameters.AddWithValue("name", labelName);

                    conn.Open();

                    try
                    {
                        insertSQL.ExecuteNonQuery();
                        rowId = conn.LastInsertRowId;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
            }

            return rowId;
        }

        private static long InsertTestSequence(string name, long devAreaId, long viewId, long ipId, long groupId)
        {
            long rowId = -1;

            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                using (SQLiteCommand insertSQL = new SQLiteCommand(
                    "INSERT INTO [Sequence] (name, developmentAreaId, viewId, ipId, groupId) VALUES (@Name, @DevAreaId, @ViewId, @IpId, @GroupId)", conn))
                {
                    insertSQL.Parameters.AddWithValue("Name", name);
                    insertSQL.Parameters.AddWithValue("DevAreaId", devAreaId);
                    insertSQL.Parameters.AddWithValue("ViewId", viewId);
                    insertSQL.Parameters.AddWithValue("IpId", ipId);
                    insertSQL.Parameters.AddWithValue("GroupId", groupId);

                    conn.Open();

                    try
                    {
                        insertSQL.ExecuteNonQuery();
                        rowId = conn.LastInsertRowId;
                    }
                    catch (Exception ex)
                    {
                        // can't find a column with the given name
                        //throw new Exception(ex.Message);
                    }
                }
            }

            return rowId;
        }

        private static long InsertTestResult(string name, long sequenceId, string currentStatus, string platform)
        {
            long rowId;

            string statusField = platform == Platform.win64.ToString() ? "currentWinStatus" : "currentLnxStatus";

            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                using (SQLiteCommand insertSQL = new SQLiteCommand($"INSERT INTO [Test] (name, sequenceId, {statusField}) VALUES (@Name, @SeqId, @Status)", conn))
                {
                    insertSQL.Parameters.AddWithValue("Name", name);
                    insertSQL.Parameters.AddWithValue("SeqId", sequenceId);
                    insertSQL.Parameters.AddWithValue("Status", currentStatus);

                    conn.Open();

                    try
                    {
                        insertSQL.ExecuteNonQuery();
                        rowId = conn.LastInsertRowId;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
            }

            return rowId;
        }

        private static void InsertTestHistory(long testId, string status, string platform, string date)
        {
            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                using (SQLiteCommand insertSQL = new SQLiteCommand("INSERT INTO [TestHistory] (testId, status, platform, date) VALUES (@TestId, @Status, @Platform, @Date)", conn))
                {
                    insertSQL.Parameters.AddWithValue("TestId", testId);
                    insertSQL.Parameters.AddWithValue("Status", status);
                    insertSQL.Parameters.AddWithValue("Platform", platform);
                    insertSQL.Parameters.AddWithValue("Date", date);

                    conn.Open();

                    try
                    {
                        insertSQL.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
            }
        }


        // update

        /* Updates data from the Storage table. */
        private static void UpdateKeyValue(string key, string value)
        {
            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                using (SQLiteCommand updateSql = new SQLiteCommand($"UPDATE {storageTableName} SET value=@Value WHERE key=@Key", conn))
                {
                    updateSql.Parameters.AddWithValue("Key", key);
                    updateSql.Parameters.AddWithValue("Value", value);

                    conn.Open();

                    try
                    {
                        updateSql.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
            }
        }

        private static void UpdateTest(long testId, string status, Platform platform, bool regression)
        {
            string statusField = platform == Platform.win64 ? "currentWinStatus" : "currentLnxStatus";
            string regressionField = platform == Platform.win64 ? "regressedWin" : "regressedLnx";

            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                using (SQLiteCommand updateSql = new SQLiteCommand($"UPDATE [Test] SET {statusField}=@Status, {regressionField}=@Regression WHERE id=@TestId", conn))
                {
                    updateSql.Parameters.AddWithValue("Status", status);
                    updateSql.Parameters.AddWithValue("TestId", testId);
                    updateSql.Parameters.AddWithValue("Regression", regression);

                    conn.Open();

                    try
                    {
                        updateSql.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
            }
        }
    }
}
