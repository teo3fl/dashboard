﻿using System;
using System.Collections.Generic;

namespace Dashboard
{
    public class XmlData
    {
        public TestSequence Sequence { get; set; }
        public List<Test> Tests { get; set; }

        public XmlData()
        {
            Sequence = new TestSequence();
            Tests = new List<Test>();
        }

        public class TestSequence
        {
            public string Platform { get; set; }
            public string Environment { get; set; }
            public string Label { get; set; }
            public string DbaseName { get; set; }
            public string SeqName { get; set; }
            public string View { get; set; }
        }

        public class Test
        {
            public string Name { get; set; }
            public string Status { get; set; }
            private string date;
            public string Date
            {
                get
                {
                    return date;
                }
                set
                {
                    date = FormatDate(value);
                }
            }

            private string FormatDate(string date)
            {
                /* The raw date extracted from the xml will look like this: Mon Jan 01 13:14:15 2021
                 
                   The string will be split into an array of strings: {"Mon", "Jan", "01", "13:14:15", "2021"}
                   The relevant strings will be used for creating the formatted string.

                   The formatted string will look like this: DD-MM-YYYY
                */

                var dateParts = date.Split(' ');

                var day = dateParts[2];

                int monthInt = DateTime.Parse("1." + dateParts[1] + " 2008").Month;
                string month = "";
                if (monthInt < 10)
                    month += '0';
                month += monthInt;

                var year = dateParts[4];

                return $"{day}-{month}-{year}";
            }
        }
    }
}
